/*
  --------------------------
  Creator: 0J3#9971
  License: AGPL 3.0 or later
  --------------------------
  Copyright (c) 2017-2020 TStudios. All Rights Reserved.
  --------------------------
*/
// Modules
const chalk   = require('chalk');
const fs      = require('fs');
const prompt  = require('prompt')
const os      = require('os-utils');

// Aliases
const print   = console.log
const log     = console.log

// Loading Message
process.stdout.write(`${chalk.rgb(25,255,25)("NJS")}${chalk.rgb(0,255,255)('SH')}\n`)

// Config
var config    = require('./conf.js') // Config changes a bit
const path    = require('./path.js')
const commanddir = './commands' // process.argv[2];
const errdict = require('./errdict.js')

// Notice Message
if (config.displayNotice) {
  process.stdout.write(`${chalk.rgb(255,50,50)("NOTICE:\n\nNJSSH will rarely be updated. \nPlease switch to another shell for the time being. \nIf you want to help make NJSSH stay alive, \n  contribute by adding commands to the commands folder and then submitting them in a pull request.\n\nThank you,\n- TStudios aka 0J3\n\n\n")}You can disable this in the config by setting displayNotice to false.\n`)
}

// Storage
const storage = require('node-persist');
async function runme() {
  try {
    // Get Value and return it
    return await storage.getItem('data');
  } catch (e) {
    try {
      await storage.setItem('data', {})
      return await storage.getItem('data');
    } catch (e) {

    }
  }
}
var storageconf = runme()
async function setdat(datname,datval) {
  storageconf[datname] = datval
  try {
    await storage.setItem('data', storageconf)
  } catch (e) {

  }
}
if (!storageconf.exists) {
  setdat('exists', true)
  setdat('shownote', true)
}

/*
  Ok that's enough defining variables for now
  Let's get to the actual code :)
*/

// Actual code
var commands = {}


fs.readdir(commanddir, function(err, items) {
    for (var i=0; i<items.length; i++) {
        commands[items[i].replace('.js', '').toLowerCase()]=require(commanddir + '/' + items[i])
        if (!commands[items[i].replace('.js', '').toLowerCase()].run) {
          log(`${chalk.bold.red("FATAL ")}${chalk.red(`Command module ${items[i].replace('.js', '').toLowerCase()} exists but has no run object. Exiting!`)}`)
          process.exit(100)
        }
    }
});
if (commands[process.argv[2]]) {
  commands[process.argv[2]].run()
}
prompt.start();
prompt.message=""
prompt.delimiter=""

if (config.terminal == "auto") {
  if (os.platform() == "linux") {
    config.terminal = '/bin/bash'
  } else if (os.platform() == "win32") {
    config.terminal = 'C:\\Windows\\System32\\cmd.exe'
  } else {
    console.log(chalk.red(`ERR: Unset Terminal for platform ${os.platform()}. Please define path manually in conf.js`))
  }
}
function run() {
  prompt.get(['>'], function (err, result) {
    try {
      // console.log(result['>'])
      if (result['>'].split(' ')[0].toLowerCase() == 'exit') {
        process.exit()
      } else if (result['>'].split(' ')[0] == '') {
        console.log(`${chalk.rgb(0,122,0)("NJS")}${chalk.rgb(255,255,255)('SH')}` + ":" + chalk.rgb(255,255,0)(' No Command Specified. Maybe the ') + chalk.rgb(0,0,255)('Help') + chalk.rgb(255,255,0)(' Command will provide you assistance?'))
      } else {
        var cmd = commands[result['>'].split(' ')[0].toLowerCase()].run
        result['>']=result['>'].split(' ')
        result['>'].shift()
        async function runcmd () {
          await cmd(result['>'])
        }
        runcmd()
      }
    } catch (e) {
      if (e) {
        if (errdict[e]) {
          process.stdout.cursorTo(0);
          process.stdout.clearLine();
          process.stdout.write(`${chalk.rgb(0,122,0)("NJS")}${chalk.rgb(255,255,255)('SH')}: ${chalk.bold.rgb(255,122,0)(errdict[e].msg)}\n`)
          if (errdict[e].exit) {
            process.exit(0)
          }
        } else {
          process.stdout.cursorTo(0);
          process.stdout.clearLine();
          process.stdout.write("An " + chalk.bold.red('Unknown') + " error occured when running this command. " + e + `\n`)
        }
      }
    }
    setTimeout(function () {
      run()
    }, 50);
  })
}
run()
