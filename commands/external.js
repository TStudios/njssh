/*
  --------------------------
  Creator: 0J3#9971
  License: AGPL 3.0 or later
  Purpose: Run External Code
    or Files from the
    specified path in the
    config.
  --------------------------
  Copyright (c) 2017-2020 TStudios. All Rights Reserved.
  --------------------------
*/
const { exec } = require('child_process');
const chalk = require('chalk')
module.exports = {
  run: function(arguments) {
    const args = arguments.join(' ')
    if (args == '' || !args) {
      return console.error("No external command specified")
    }
    exec(args, (err, stdout, stderr) => {
      if (err) {
        console.error(`An error has occured while running external command ${args}`)
        if (stdout != "") {
          process.stdout.write(`${stdout}`);
        }
        if (stderr != "") {
          console.log(chalk.red(`${stderr}`));
        }
      } else {
        if (stdout != "") {
          process.stdout.write(`${stdout}`);
        }
        if (stderr != "") {
          console.log(chalk.red(`${stderr}`));
        }
      }
    });
  }
}
