const os    = require('os-utils');
const chalk = require('chalk');
module.exports = {
  run: async function(args) {
    var x = chalk.bold('System Info:')
    function add(text,clr) {
      var color = clr
      if (color == "lightblue") {
        x=x+"\n"+chalk.rgb(0,255,255)(text)
      } else {
        x=x+"\n"+chalk[color](text)
      }
    }
    add(`|-- OS:`, 'red')
    add(`|   |-- Platform: ${os.platform()}`,'red')
    add(`|   |-- Uptime: ${Math.floor(os.sysUptime()/60/60)} Hours, ${Math.floor(os.sysUptime()/60)-Math.floor(os.sysUptime()/60/60)*60} Minutes, ${os.sysUptime()-Math.floor(os.sysUptime()/60)*60-Math.floor(os.sysUptime()/60/60)} Seconds`, 'red')
    add(`|   |-- Process Uptime: ${Math.round(os.processUptime()*100)/100} Seconds`, 'red')
    add(`|`, 'yellow')
    add(`|-- CPU:`,'green')
    add(`|   |-- Core Count: ${os.cpuCount()}`,'green')
    add(`|   |-- Average Load:`, 'green')
    add(`|   |   |-- 1 Minute: ${Math.round(os.loadavg(1)*10)/10}%`, 'green')
    add(`|   |   |-- 2.5 Minutes: ${Math.round(os.loadavg(2.5)*10)/10}%`, 'green')
    add(`|   |   |-- 5 Minutes: ${Math.round(os.loadavg(5)*10)/10}%`, 'green')
    add(`|   |   |-- 10 Minutes: ${Math.round(os.loadavg(10)*10)/10}%`, 'green')
    add(`|   |   |-- 15 Minutes: ${Math.round(os.loadavg(15)*10)/10}%`, 'green')
    add(`|   |   |-- 30 Minutes: ${Math.round(os.loadavg(30)*10)/10}%`, 'green')
    add(`|   |   |-- 1 Hour: ${Math.round(os.loadavg(60)*10)/10}`, 'green')
    // await os.cpuUsage(function(value) {
    //   add(`    Usage: ${value}%`)
    // })
    // await os.cpuFree(function(value) {
    //   add(`    Free: ${value}%`)
    // })
    add(`|   |-- Usage: Unknown (Callback required, causes slow command)`,'green')
    add(`|   |-- Free: Unknown (Callback required, causes slow command)`,'green')
    add(`|`, 'lightblue')
    add(`|-- Memory:`,'blue')
    add(`    |-- Free Memory: ${Math.round(os.freememPercentage()*10)/10}% (${Math.round(os.freemem()/100)/10}GB/${Math.round(os.totalmem()/100)/10}GB Free)`, 'blue')
    add(`    |-- Used Memory: ${100-Math.round(os.freememPercentage()*10)/10}% (${Math.round(os.totalmem()-os.freemem()/100)/10}GB Free)`, 'blue')
    console.log(x)
  }
}
