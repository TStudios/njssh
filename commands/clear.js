/*
  --------------------------
  Creator: 0J3#9971
  License: AGPL 3.0 or later
  Purpose: Clear console
    using multiple methods
    incase one doesn't work
  --------------------------
  Copyright (c) 2017-2020 TStudios. All Rights Reserved.
  --------------------------
*/
const { exec } = require('child_process');
const chalk = require('chalk')
module.exports = {
  run: function(arguments) {
    var lines = process.stdout.getWindowSize()[1];
    for(var i = 0; i < lines; i++) {
        console.log('\r\n');
    }
    console.clear()
  }
}
