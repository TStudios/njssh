const chalk    = require('chalk');
module.exports = {
  run: function(args) {
    process.stdout.write(`Important built in commands:\n  ${chalk.red('Exit')} - Exit the shell\n  ${chalk.blue('Help')} - This`)
    process.stdout.write("\n")
  }
}
