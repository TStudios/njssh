/*
  --------------------------
  Creator: 0J3#9971
  License: AGPL 3.0 or later
  Purpose: Run raw js using
    eval
  --------------------------
  Copyright (c) 2017-2020 TStudios. All Rights Reserved.
  --------------------------
*/
const { exec } = require('child_process');
const chalk = require('chalk')
module.exports = {
  run: function(arguments) {
    eval(arguments.join(' '))
  }
}
